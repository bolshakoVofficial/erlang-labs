-module(proc_sieve).

-export([generate/1]).
-export([gen_print/1]).
-export([sieve/0]).


% Вызывает функцию generate() для вычисления всех простых чисел до MaxN
gen_print(MaxN) ->
	lists:foreach(fun(X)->io:format("~w ",[X]) end, generate(MaxN)).


% возвращает список всех простых чисел от 2 до MaxN, включительно
generate(MaxN) when MaxN > 1 ->
	PID = spawn(proc_sieve, sieve, []),
	generate_aux(2, MaxN, PID);

generate(_) ->
	[].

generate_aux(CurNum, MaxN, PID) when CurNum =< MaxN ->
	PID ! CurNum,
	generate_aux(CurNum + 1, MaxN, PID);

generate_aux(_, _, PID) ->
	PID ! {done, self()},
	receive
		{result, List} ->
			List;
		_ ->
			{error, "Wrong output"}
	end.


% решето отфильтровывает числа делящиеся на N
sieve() ->
	receive
		FilterBy ->
			sieve_aux(FilterBy, nil, nil)
	end.

sieve_aux(Filter, RedirectPid, SavedBackPID) ->
	receive
		{done, BackPID} ->
			if
				RedirectPid == nil ->
					BackPID ! {result, [Filter]};
				true ->
					RedirectPid ! {done, self()},
					sieve_aux(Filter, nil, BackPID)
			end;
		{result, List} ->
			if
				SavedBackPID =/= nil ->
						SavedBackPID ! {result, [Filter | List]};
				true ->
					nil
			end;
		CurNum when (CurNum rem Filter) =/= 0 ->
			if
				RedirectPid == nil ->
					PID = spawn(proc_sieve, sieve, []),
					PID ! CurNum,
					sieve_aux(Filter, PID, SavedBackPID);
				true ->
					RedirectPid ! CurNum,
					sieve_aux(Filter, RedirectPid, SavedBackPID)
			end;
		_ ->
			sieve_aux(Filter, RedirectPid, SavedBackPID)
	end.
