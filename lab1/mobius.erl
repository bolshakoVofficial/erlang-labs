-module(mobius).

-export([is_prime/1]).
-export([prime_factors/1]).
-export([is_square_multiple/1]).
-export([find_square_multiples/2]).


% проверяем, простое ли число
is_prime(N) ->
	is_prime_aux(2, N).

is_prime_aux(Divisor, N) when (Divisor * Divisor) =< N ->
	if
		(N rem Divisor) == 0 ->
			false;
		true ->
			is_prime_aux(Divisor + 1, N)
	end;

is_prime_aux(_, _) -> true.


% возвращает список простых сомножителей N
prime_factors(0) ->
	[];

prime_factors(N) ->
	prime_factors_aux(N, 2, [1]).

prime_factors_aux(N, CoMultiplier, CurResult) when CoMultiplier =< N ->
	if
		(N rem CoMultiplier) == 0 ->
			IsPrime = is_prime(CoMultiplier),
			if 
				IsPrime == true ->
					prime_factors_aux(N, CoMultiplier + 1, [CoMultiplier|CurResult]);
				true ->
					prime_factors_aux(N, CoMultiplier + 1, CurResult)
			end;
		true ->
			prime_factors_aux(N, CoMultiplier + 1, CurResult)
	end;

prime_factors_aux(_, _, Result) ->
	Result.


% возвращает true, если аргумент делится на квадрат простого числа
% возвращает false, если не длится
is_square_multiple(N) when N < 4 ->
	false;

is_square_multiple(N) ->
	is_square_multiple_aux(N, 2).

is_square_multiple_aux(N, CoMultiplier) when CoMultiplier =< N ->
	if
		(N rem CoMultiplier) == 0 ->
			IsPrime = is_prime(CoMultiplier),
			if 
				IsPrime == true ->
					if
						(N rem (CoMultiplier * CoMultiplier)) == 0 ->
							true;
						true ->
							is_square_multiple_aux(N, CoMultiplier + 1)
					end;
				true ->
					is_square_multiple_aux(N, CoMultiplier + 1)
			end;
		true ->
			is_square_multiple_aux(N, CoMultiplier + 1)
	end;

is_square_multiple_aux(_, _) ->
	false.


% ищет первый ряд из чисел делящихся на квадрат простого числа длиной Count
find_square_multiples(Count, MaxN) ->
	find_square_multiples_aux(0, failure, 0, Count, MaxN).

find_square_multiples_aux(CurNum, FirstInLine, FoundNum, NeedFind, SearchLimit) 
when CurNum =< SearchLimit , FoundNum < NeedFind ->
	IsSuitable = is_square_multiple(CurNum),
	if
		IsSuitable == true ->
			if
				FirstInLine == failure ->
					find_square_multiples_aux(CurNum + 1, CurNum, FoundNum + 1, NeedFind, SearchLimit);
				true ->
					find_square_multiples_aux(CurNum + 1, FirstInLine, FoundNum + 1, NeedFind, SearchLimit)
			end;
		true ->
			find_square_multiples_aux(CurNum + 1, failure, 0, NeedFind, SearchLimit)
	end;

find_square_multiples_aux(_, FirstInLine, FoundNum, NeedFind, _) when FoundNum == NeedFind ->
	FirstInLine;

find_square_multiples_aux(_, _, _, _, _) ->
	failure.

