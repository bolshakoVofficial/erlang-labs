-module(rss_reader).
-include("logging.hrl").
-compile(export_all).
-define(RETRIEVE_INTERVAL,200000).

%% @doc start(Url, QPid) функция запускает новый процесс, 
%% вызывая функцию server(Url, QPid), 
%% которая реализует основной цикл процесса rss_reader
start(Url,QPid)->
  inets:start()
  ,spawn(?MODULE,server,[Url,QPid]).

server(Url,QPid)->
%% @doc загрузка ленты с указанного URL, с помощью функции httpc:request/1.
  {ok,{Status={_,Code,_},_,Load}}=httpc:request(Url)

%% @doc если код ответа равен 200, разбираем его XML содержимое с помощью 
%% функции xmerl_scan:string/1.
  ,case Code of 
    200 ->
       {Feed,_} = xmerl_scan:string(Load)

%% @doc проверяем, что получена лента в формате RSS 2.0.
%% @see rss_parse:is_rss2_feed
       ,case rss_parse:is_rss2_feed(Feed) of
        ok -> 

%% @doc отправляем все элементы ленты в очередь, 
%% которая стоит в паре с этим процессом чтения
%% @see rss_queue:add_feed
          rss_queue:add_feed(QPid,Feed)
          ,receive
%% @doc Ждем заданное время
            after ?RETRIEVE_INTERVAL -> 
              server(Url,QPid)
            end;
        _ -> {error,not_rss2_feed}
        end ; 
    _ -> {error,Code}
   end.